%%
clear;
clc;
A = [1, -4.25];
B = [-1,4.25];
C= [-4.25, 1];
D = [4.25,-1];
slope = -4;
slope2 = +1;
d1 = slope;
d2= slope;



[a,b,c,d,X] = show_cube(C,D, d1, d2);
f= @(x) ( (x.^3).*a + (x.^2).*b + (x).*c + d );
[a1, b1, c1, d1] = Derivada(a, b, c, d);
h= @(x) ( (x.^3).*a1 + (x.^2).*b1 + (x).*c1 + d1 );
[a2, b2, c2, d2] = Derivada(a1, b1, c1, d1);
g= @(x) ( (x.^3).*a2 + (x.^2).*b2 + (x).*c2 + d2 );
radios = RadioDeCurvatura(h,g);
j = tangentes([2.36,-2.36]);
velmax = VelocidadesMax(radios);

%recorridoNormal(f,velmax);
recorridoTangente(f,j,velmax);

%%
function [a,b,c,d,X] = show_cube(MIN, MAX, d1, d2)
hold on;

a=0.10421331162222674536942804803582;
b =0;
c = -1.6470588235294117647058823529412;
d = 0;

%[a,b,c] = solve(a*64+16*b+c*4 ==-4, a*(-64)+b*(16)+c*(-4)==4, 48*a+(-8)*b+c==0);
fn = @(x_,x,y,z) x*x_.^3+y*x_.^2+z.*x_;
df = @(x) (1+(3*double(a)*x.^2+ 2*double(b)*x+double(c)).^2).^(1/2);

arco = integral(df, -4.25, 4.25);
disp(abs(arco));
Carril_B = linspace(MIN(1), MIN(1)-.5, 50);
Carril_A = linspace(MAX(1)+0.5, MAX(1), 50);


bA = MAX(2)-MAX(1)*d1;
bB = MIN(2)-MIN(1)*d2;
Ay = Carril_A.*d1+bA;
By = Carril_B.*d2+bB;

X = linspace(MIN(1),MAX(1), 50);
Y = fn(X,vpa(a),vpa(b),vpa(c));
%DY = df(X, vpa(a), vpa(b), vpa(c));
scatter (Carril_A,Ay*-1);
scatter( Carril_B,By*-1);
plot(X,Y);
%plot([MIN(2), MAX(2)],[MIN(1), MAX(1)]);
%scatter(X,DY);
grid on;

[a1, b1, c1, d1] = Derivada(a, b, c, d);
disp("f'(x)= "+num2str(a1)+"x^3 "+ num2str(b1)+"x^2 "+ num2str(c1)+"x "+ num2str(d1));
[a2, b2, c2, d2] = Derivada(a1, b1, c1, d1);
disp("f''(x)= "+num2str(a2)+"x^3 "+ num2str(b2)+"x^2 "+ num2str(c2)+"x "+ num2str(d2));

end


%Tangentes y = f'(p)*x-f'(p)*p+f(p)
function j = tangentes(p)

    a=0.10421331162222674536942804803582;
    b =0;
    c = -1.6470588235294117647058823529412;
    d = 0;
    [a1, b1, c1, d1] = Derivada(a, b, c, d);
    f= @(x) ( (x.^3).*a1 + (x.^2).*b1 + (x).*c1 + d1 );
    fn = @(x) a*x.^3+c*x;
    x = [-4.25 4.25];
    for i=1:length(p)
        y = f(p(i)).*x-f(p(i))*p(i)+fn(p(i));
        plot(x,y)
    end
    w = f(-2.36);
    u = fn(-2.36);
    j = @(x) w.*x-w*-2.36+u;
end

%Velocidades Máximas
function velocidades = VelocidadesMax(r)
    velocidades = ( r * 50 .* 9.8 .* 0.75).^(1/2);
end

%Carlos Radio
function radios = RadioDeCurvatura(f,g)
    x = linspace(4.25,-4.25,426);
    radios = ((1 + (f(x).^2)).^(3/2)) ./ abs(g(x));
end

function recorridoNormal(f, v)
    x = linspace(-4.25,4.25,426);
    TextoX = -3; TextoY = 0;
    text(TextoX, TextoY,['Posicion inicial = [' num2str(x(1)) ',' num2str(f(x(1))) ']']);
    text(TextoX, TextoY-.25,['Posicion final = [' num2str(x(length(x))) ',' num2str(f(x((length(x))))) ']']);
    t0 = text(TextoX, TextoY-0.5,['X = ' num2str(x(1) * 50) ' m' ]);
    t1 = text(TextoX, TextoY-0.75,['Y = ' num2str(f(x(1))* 50) ' m' ]);
    t2 = text(TextoX, TextoY-1,['Vel Max = ' num2str(v(1) * .001 * 3600) ' km/hr' ]);
    for i = 1:length(x)
        delete(t0); delete(t1); delete(t2);
        t0 = text(TextoX, TextoY-0.5,['X = ' num2str(x(i) * 50) ' m' ]);
        t1 = text(TextoX, TextoY-0.75,['Y = ' num2str(f(x(i))*50) ' m']);
        t2 = text(TextoX, TextoY-1,['Vel Max = ' num2str(v(i) * .001 *3600) ' km/hr' ]);
        plot(x(i),f(x(i)),"o r")
        pause(0.1)
        plot(x(i),f(x(i)),"o w")
    end
end

function recorridoTangente(f,h,v)
    x = linspace(-4.25,4.25,100);
    TextoX = -3; TextoY = 0;
    text(TextoX, TextoY,['Posicion inicial = [' num2str(x(1)) ',' num2str(f(x(1))) ']']);
    text(TextoX, TextoY-.25,['Posicion final = [' num2str(x(length(x))) ',' num2str(f(x((length(x))))) ']']);
    t0 = text(TextoX, TextoY-0.5,['X = ' num2str(x(1) * 50) ' m' ]);
    t1 = text(TextoX, TextoY-0.75,['Y = ' num2str(f(x(1))* 50) ' m' ]);
    t2 = text(TextoX, TextoY-1,['Vel Max = ' num2str(v(1) * .001 * 3600) ' km/hr' ]);
    for i = 1:length(x)
        if x(i) >= -2.36
            f = h;
            v(i:426) = v(i);
        end
        delete(t0); delete(t1); delete(t2);
        t0 = text(TextoX, TextoY-0.5,['X = ' num2str(x(i) * 50) ' m' ]);
        t1 = text(TextoX, TextoY-0.75,['Y = ' num2str(f(x(i))*50) ' m']);
        t2 = text(TextoX, TextoY-1,['Vel Max = ' num2str(v(i) * .001 *3600) ' km/hr' ]);
        plot(x(i),f(x(i)),"o r")
        pause(0.1)
        plot(x(i),f(x(i)),"o w")
    end
    
end
%% 
function [a1, b1, c1, d1] = Derivada(a,b,c,d)
a1=0;
b1 = a*3;
c1 = b*2;
d1 = c;
end